'use strict';
var selected_index = -1;
function saveUser() {
    const firstname = document.getElementById('firstname').value;
    const lastname = document.getElementById('lastname').value;
    const phone = document.getElementById('phone').value;
    const username = document.getElementById('user').value;
    const pass = document.getElementById('pass').value;
    const rides = "0";
    const user = {
        firstname,
        lastname,
        phone,
        username,
        pass,
        rides
    };
    const users = insertToTable('user', user);
    cleanInputRegister();
    // render the books
    //renderTable('user', users);
}
function saveRides() {
    const pride = document.getElementById('rideName').value;
    const start = document.getElementById('start').value;
    const end = document.getElementById('end').value;
    const description = document.getElementById('description').value;
    const departure = document.getElementById('departure').value;
    const arrive = document.getElementById('arrive').value;
    const user = 'user';
    const ride = {
        pride,
        start,
        end,
        description,
        departure,
        arrive,
        user
    };
    const rides = insertToTable('rides', ride);
    // render the books
    cleanInput();
    renderTable('rides', rides);
    
}
function cleanInput(){
    $('#rideName').val('');
    $('#start').val('');
    $('#end').val('');
    $('#description').val('');
    $('#departure').val('');
    $('#arrive').val('');
}
function cleanInputRegister(){
    $('#firstname').val('');
    $('#lastname').val('');
    $('#phone').val('');
    $('#user').val('');
    $('#pass').val('');
}

// /**
//  * Renders an HTML table dinamically
//  *
//  * @param tableName
//  * @param tableData
//  */
function renderTable(tableName, tableData) {
    let table = jQuery(`#${tableName}_table`);
    // loop through all the items of table and generates the html
    let rows = "";
    tableData.forEach((rides, index) => {
        let row = `<tr><td>${rides.pride}</td><td>${rides.start}</td><td>${rides.end}</td><td>${rides.description}</td><td>${rides.departure}</td><td>${rides.arrive}</td><td>${rides.user}`;
        row += `<td> <a onclick="valueEdit(this)" class='btnEdit' data-id="${rides.id}" data-entity="${tableName}">Edit</a>  |  <a  onclick="deleteEntity(this);" data-id="${rides.id}" data-entity="${tableName}" class="link delete">Delete</a>  </td>`;
        rows += row + '</tr>';
    });
    table.html(rows);
}
function renderTableIndex(tableName, tableData) {
    let table = jQuery(`#${tableName}_table`);
    // loop through all the items of table and generates the html
    let rows = "";
    tableData.forEach((rides, index) => {
        let row = `<tr><td>${rides.pride}</td><td>${rides.start}</td><td>${rides.end}</td><td>${rides.description}</td><td>${rides.departure}</td><td>${rides.arrive}</td><td>${rides.user}`;        rows += row + '</tr>';
    });
    table.html(rows);
}
function check() {
    // entered data from the login-form
    var userName = document.getElementById('loginuser');
    var userPw = document.getElementById('loginpass');
    var localusers = [];
    localusers = localStorage.getItem('user');
    var users = JSON.parse(localusers);
    users.forEach(element => {
        if (userName.value == element.username && userPw.value == element.pass) {
            alert("Succes");
            document.getElementById("checkLogin").href = "dash.html";
        }
    });
}
function search() {
    var tblnewsearch =[];
    var searchRide = [];
    var searchForm =  document.getElementById("from").value;
    var searchTo =  document.getElementById("to").value;
    searchRide = localStorage.getItem('rides');
    var rides = JSON.parse(searchRide);
    rides.forEach(element => {
        if (element.start == searchForm || element.end == searchTo) {

            
             tblnewsearch.push(element);
             var caca =1;
             
            
 
        }
        renderTableIndex('rides', tblnewsearch)
    });
}
function editFromTable(element) {
    saveRides();
    cleanInput();
    document.getElementById("btnSaveEdit").style.visibility = "hidden"; 
    document.getElementById("btnSave").style.visibility = "visible";
    document.getElementById("rideName").disabled = false;
    return true;
}

// function editFromTable(element) {
//     // Editar el item seleccionado en la tabla
//     var selected_index = -1;
//     var tblRides = localStorage.getItem("rides"); //Retornar los datos almacenados
//     tblRides = JSON.parse(tblRides); //Convertir String a Object
//     var ridename = document.getElementById('rideName');
//     tblRides.forEach(element => {
//         if (element.pride == ridename.value) {
//             selected_index=element.id-1
//         }
//     });
//     tblRides[selected_index] = JSON.stringify({
//         pride: $("#ridename").val(),
//         start: $("#start").val(),
//         end: $("#end").val(),
//         description: $("#description").val(),
//         departure: $("#departure").val(),
//         arrive: $("#arrive").val()
//     });
//     //Almacenar los datos en el Local Storage
//     localStorage.setItem("rides", JSON.stringify(tblRides));
//     alert("Los datos han sido editados"); //Mensaje de alerta
//    // renderTable('rides', rides);
//     cleanInput();
//     return true;
// }


function valueEdit(element) {
     
    const dataObj = jQuery(element).data();
    var selected_index = parseInt(dataObj.id)-1;
    var tblRides = localStorage.getItem("rides"); //Retornar los datos almacenados
    tblRides = JSON.parse(tblRides); //Convertir String a Object
    if (tblRides === null) // Si no hay datos, inicializar un array vacio
        tblRides = [];
    // Convertir de JSON al formato adecuando para editarlos datos
    
    var per = tblRides[selected_index];
    $("#rideName").val(per.pride);
    $("#start").val(per.start);
    $("#end").val(per.end);
    $("#description").val(per.description);
    $("#departure").val(per.departure);
    $("#arrive").val(per.arrive);
    document.getElementById("btnSaveEdit").style.visibility = "visible"; 
    document.getElementById("btnSave").style.visibility = "hidden";
    document.getElementById("rideName").disabled = true;
    const newEntities = deleteFromTable(dataObj.entity, dataObj.id);
    renderTable(dataObj.entity, newEntities);

    return true;
}

function deleteEntity(element) {
    const dataObj = jQuery(element).data();
    var newid = document.getElementById("rideName").value;
    const newEntities = deleteFromTable(dataObj.entity, newid);
    renderTable(dataObj.entity, newEntities);
}
function loadTableData(tableName) {
    renderTable(tableName, getTableData(tableName));
}
function loadTableDataIndex(tableName) {
    renderTableIndex(tableName, getTableData(tableName));
}
/**
 * Binds the different events to the different elements of the page
 */
function bindEvents() {
    jQuery('#add-user-button').bind('click', (element) => {
        saveUser();
    });
}
function login() {
    jQuery('#checkLogin').bind('click', (element) => {
        check();
    });
}
function rides() {
    jQuery('#btnSave').bind('click', (element) => {
        saveRides();
    });
}
function edit() {
    jQuery('#btnSaveEdit').bind('click', (element) => {
        editFromTable();
    });
}
function searchIndex() {
    jQuery('#btnSearch').bind('click', (element) => {
        search();
    });
}
bindEvents();
login();
rides();
edit();
searchIndex();

